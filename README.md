# Chapel Compiler Prototype #

A simple compiler for the Chapel language running on a NodeJS server.

### How to run ###

* Clone the repository to your system.
* Ensure that you have node and npm installed by running `node -v` and `npm -v` respectively.
* Go to the root of the repo.
* Run `npm install` (you may need sudo access for this).
* Run `npm start`.
